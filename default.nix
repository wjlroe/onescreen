
let
  pkgs = import <nixpkgs> {};
  stdenv = pkgs.stdenv;
  haskellPackages = pkgs.haskellPackages;
  inherit (haskellPackages) elmReactor elmMake elmCompiler elmPackage elmRepl;

in

stdenv.mkDerivation rec {
  name = "onescreen";
  version = "0.0.1";
  src = ./.;
  buildInputs = [ elmReactor elmMake elmCompiler elmPackage elmRepl ];
  meta = with stdenv.lib; {
    description = "Ludum Dare 31 game";
    maintainers = with maintainers; [ wjlroe ];
    platforms = platforms.unix;
  };
}
