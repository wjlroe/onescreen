module Main exposing (..)

import AnimationFrame exposing (diffs)
import Char exposing (KeyCode)
import Color exposing (..)
import Collage exposing (..)
import Element exposing (..)
import Keyboard
import List exposing (..)
import Html
import Time exposing (..)


type alias Direction =
    { x : KeyCode, y : KeyCode }


type alias Delta =
    Float


type alias Dimentions =
    ( Int, Int )



-- type alias InputSignals =
--     ( Delta, Direction, Dimentions )


type Msg
    = TimeUpdate Time
    | KeyDown KeyCode
    | KeyUp KeyCode


type alias Position a =
    { a | x : Int, y : Int }


type alias Coordinates =
    { x : Float, y : Float }


type alias Velocity a =
    { a | vx : Float, vy : Float }


type alias Object a =
    { a | x : Float, y : Float, vx : Float, vy : Float }


type alias Robot =
    Position (Velocity {})


type alias Size a =
    { a | width : Int, height : Int }


type alias HasRobot a =
    { a | sqSize : Int, rSizeF : Int, robot : Robot }


type alias Model =
    Size (HasRobot {})


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ AnimationFrame.diffs TimeUpdate
        , Keyboard.downs KeyDown
        , Keyboard.ups KeyUp
        ]


robot : Robot
robot =
    { x = 0, y = 0, vx = 0, vy = 0 }


world : Model
world =
    { width = 5, height = 10, robot = robot, sqSize = 0, rSizeF = 0 }


model : Model
model =
    world


verticalSpeed : Float
verticalSpeed =
    5


horizontalSpeed : Float
horizontalSpeed =
    10


imageSize : Int
imageSize =
    64


scaleFactor : Int
scaleFactor =
    2


type Key
    = Unknown


fromCode : KeyCode -> Key
fromCode keyCode =
    case keyCode of
        _ ->
            Unknown


keyDown : KeyCode -> Model -> Model
keyDown keyCode model =
    case fromCode keyCode of
        _ ->
            model


keyUp : KeyCode -> Model -> Model
keyUp keyCode model =
    case fromCode keyCode of
        _ ->
            model


inBounds : Model -> Dimentions -> Robot -> Bool
inBounds world ( w, h ) r =
    let
        ( wf, hf ) =
            ( toFloat w, toFloat h )

        sqSize =
            world.sqSize

        sqSizeF =
            toFloat world.sqSize
    in
        ((sqSize - h // 2) <= r.y)
            && (r.y <= (h // 2))
            && ((world.rSizeF - w // 2) <= r.x)
            && (r.x < (w // 2 - world.rSizeF))


worldSize : Dimentions -> Model -> Model
worldSize ( w, h ) world =
    let
        mHeight =
            h // world.height

        mWidth =
            w // world.width

        sqSize =
            min mHeight mWidth
    in
        { world
            | sqSize = sqSize
            , rSizeF = sqSize // 2
        }


jump : Dimentions -> Direction -> Model -> Model
jump wd { y } world =
    let
        robot =
            world.robot

        newbot =
            { robot | vy = (toFloat y) * verticalSpeed }
    in
        { world | robot = newbot }


walk : Dimentions -> Direction -> Model -> Model
walk wd { x } world =
    let
        robot =
            world.robot

        newbot =
            { robot | vx = (toFloat x) * horizontalSpeed }
    in
        { world | robot = newbot }


physics : Dimentions -> Delta -> Model -> Model
physics wh t world =
    let
        robot =
            world.robot

        new =
            { robot
                | x = robot.x + round (t * robot.vx)
                , y = robot.y + round (t * robot.vy)
            }

        reset =
            { robot | vx = 0, vy = 0 }
    in
        { world
            | robot =
                if inBounds world wh new then
                    new
                else
                    reset
        }


updatePhysics : Time -> Model -> Model
updatePhysics dt model =
    physics ( model.width, model.height ) dt model


step : ( Delta, Direction, Dimentions ) -> Model -> Model
step ( dt, keys, wh ) =
    worldSize wh >> jump wh keys >> walk wh keys >> physics wh dt



--scaleForScreen : Model -> Float
--scaleForScreen world =
-- x,y coord -> scale/move ???


floorTiles : Dimentions -> Model -> Form
floorTiles ( w, h ) world =
    let
        sqSize =
            world.sqSize

        newCoords x =
            ( toFloat (sqSize * x - w // 2)
            , toFloat (sqSize - h // 2)
            )

        floorTile x =
            toForm (image imageSize imageSize "../assets/imgs/floor.gif")
                |> scale (toFloat scaleFactor)
                |> move (newCoords x)
    in
        group <| map floorTile (List.range 0 (w // sqSize))


coordinatesOfObject : Model -> Position a -> Coordinates
coordinatesOfObject { sqSize } { x, y } =
    { x = toFloat <| sqSize * x
    , y = toFloat <| sqSize * y
    }


gridReferences : Model -> List ( Int, Int )
gridReferences { width, height } =
    concatMap (\x -> map (\y -> ( x, y )) (List.range 0 (height - 1))) (List.range 0 (width - 1))


gridSquares : Float -> Float -> Model -> Form
gridSquares w h world =
    let
        { width, height } =
            world

        sqSize =
            toFloat world.sqSize

        minY =
            if height % 2 == 0 then
                -sqSize * (toFloat ((height - 1) // 2) + 0.5)
            else
                -sqSize * (toFloat (height // 2))

        minX =
            if width % 2 == 0 then
                -sqSize * (toFloat ((width - 1) // 2) + 0.5)
            else
                -sqSize * (toFloat (width // 2))

        coords =
            gridReferences world

        square ( x, y ) =
            let
                ( xf, yf ) =
                    ( toFloat x, toFloat y )
            in
                rect sqSize sqSize
                    |> outlined { defaultLine | width = 1 }
                    |> move ( xf * sqSize + minX, yf * sqSize + minY )
    in
        group <| map square coords


draw : Dimentions -> Model -> Element
draw ( w, h ) world =
    let
        ( wf, hf ) =
            ( toFloat w, toFloat h )

        robotImg =
            "../assets/imgs/robot.gif"

        rSize =
            world.rSizeF
    in
        collage w
            h
            [ rect wf hf |> filled (rgb 179 179 179)
            , rect 4 4 |> filled (rgb 10 10 10)
            , rect 20 20
                |> outlined { defaultLine | width = 1 }
                |> move ( 0, -10 )
            , floorTiles ( w, h ) world
            , toForm (image rSize rSize robotImg)
                |> scale (toFloat scaleFactor)
                |> move
                    ( toFloat robot.x
                    , toFloat (robot.y + rSize + (16 * scaleFactor) - h // 2)
                    )
            , gridSquares wf hf world
            ]


view : Model -> Html.Html msg
view model =
    draw ( 100, 100 ) model |> toHtml



-- input : Signal InputSignals
-- input =
--     let
--         delta =
--             Signal.map (\t -> t / 20) (fps 25)
--         directions =
--             Signal.merge Keyboard.arrows Keyboard.wasd
--         inputSignals =
--             Signal.map3 (,,) delta directions Window.dimensions
--     in
--         Signal.sampleOn delta inputSignals
-- main : Signal Element
-- main =
--     Signal.map2 draw Window.dimensions (Signal.foldp step world input)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        TimeUpdate dt ->
            ( updatePhysics dt model, Cmd.none )

        KeyDown keyCode ->
            ( keyDown keyCode model, Cmd.none )

        KeyUp keyCode ->
            ( keyUp keyCode model, Cmd.none )


init : ( Model, Cmd Msg )
init =
    ( model, Cmd.none )


main =
    Html.program
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }
