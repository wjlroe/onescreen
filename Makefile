source_files := $(wildcard *.elm)
build_files := $(source_files:%.elm=build/%.html)
PATH := "C:\Program Files (x86)\Elm Platform\0.18\bin:$(PATH)"

build/%.html: %.elm
	elm make $< --output $@ --yes

all: $(build_files)
